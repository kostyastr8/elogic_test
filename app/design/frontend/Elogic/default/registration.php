<?php
/**
 * Copyright (c) 2017.
 * Created by Magento2 Developer Plekanets K. for Elogic Test Task
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/Elogic/default',
    __DIR__
);
