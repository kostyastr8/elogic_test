<?php
/**
 * Copyright (c) 2017.
 * Created by Magento2 Developer Plekanets K. for Elogic Test Task
 */

namespace Elogic\Providers\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;

class InstallData implements InstallDataInterface
{
    /**
     * @var $eavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Install eav_attribute
     * @var  $context
     * @var  $setup
     * @return void
     */

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            Product::ENTITY,
            "providers",
            [
                'group' => 'general',
                'type' => 'int',
                'label' => 'Provider',
                'note' => '',
                'input' => "select",
                'source' => "Elogic\Providers\Model\Source\Provider",
                'backend' => "Elogic\Providers\Model\Attribute\Backend\Provider",
                'required' => false,
                'sort_order' => 50,
                'global' => Attribute::SCOPE_STORE,
                'is_html_allowed_on_front' => true,
                'visible' => true,
                'is_visible_in_grid' => false,
                'is_used_in_grid' => false,
                'is_filterable_in_grid' => false,
                'user_defined' => false,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                "used_in_product_listing" => true,
            ]
        );
    }
}