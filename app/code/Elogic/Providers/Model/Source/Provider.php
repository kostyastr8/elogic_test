<?php
/**
 * Copyright (c) 2017.
 * Created by Magento2 Developer Plekanets K. for Elogic Test Task
 */

namespace Elogic\Providers\Model\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Provider extends AbstractSource
{
    /**
     * @var \Elogic\Providers\Model\Provider
     */
    protected $_provider;

    /**
     * Constructor
     *
     * @param \Elogic\Providers\Model\Provider $provider
     */
    public function __construct(\Elogic\Providers\Model\Provider $provider)
    {
        $this->_provider = $provider;
    }

    /**
     * @var  $_options
     */
    protected $_options;

    /**
     * Get options
     * @return array
     */
    public function getAllOptions()
    {
        $_options[] = ['label' => '', 'value' => ''];
        $providerCollection = $this->_provider->getCollection()
            ->addFieldToSelect('entity_id')
            ->addFieldToSelect('name');
        foreach ($providerCollection as $provider) {
            $_options[] = [
                'label' => $provider->getName(),
                'value' => $provider->getId(),
            ];
        }
        return $_options;
    }
}