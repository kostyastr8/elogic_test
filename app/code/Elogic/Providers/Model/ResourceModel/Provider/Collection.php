<?php
/**
 * Copyright (c) 2017.
 * Created by Magento2 Developer Plekanets K. for Elogic Test Task
 */

namespace Elogic\Providers\Model\ResourceModel\Provider;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use \Elogic\Providers\Model\Provider;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = provider::PROVIDER_ID;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Elogic\Providers\Model\Provider', 'Elogic\Providers\Model\ResourceModel\Provider');
    }

}