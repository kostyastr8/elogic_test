<?php
/**
 * Copyright (c) 2017.
 * Created by Magento2 Developer Plekanets K. for Elogic Test Task
 */

namespace Elogic\Providers\Model\Attribute\Backend;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;
use Magento\Framework\Exception\LocalizedException;

class Provider extends AbstractBackend
{
    const PROVIDER_EXCEPTION = 'test3';
    /**
     * @param Product $object
     * @throws LocalizedException
     * @return void
     */
    public function validate($object)
    {
        $value = $object->getData($this->getAttribute()->getAttributeCode());
        if (($object->getAttributeSetId()== 15) && ($value == self::PROVIDER_EXCEPTION)) {
            throw new LocalizedException(__('Exceptions'));
        }
    }
}