<?php
/**
 * Copyright (c) 2017.
 * Created by Magento2 Developer Plekanets K. for Elogic Test Task
 */

namespace Elogic\Providers\Controller\Adminhtml\Provider;

use Magento\Backend\App\Action;
use Elogic\Providers\Model\Provider;
class Delete extends Action
{
    /**
     * @var \Elogic\Providers\Model\Provider $model
     */
    protected $_model;

    /**
     * @param Action\Context $context
     * @param \Elogic\Providers\Model\Provider $model
     */
    public function __construct(
        Action\Context $context,
        Provider $model
    ) {
        parent::__construct($context);
        $this->_model = $model;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Elogic_Providers::provider_delete');
    }

    /**
     * Delete action
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_model;
                $model->getResource()->load($model, $id);
                $model->getResource()->delete($model);
                $this->messageManager->addSuccessMessage(__('Providers deleted'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('Provider does not exist'));
        return $resultRedirect->setPath('*/*/');
    }
}